package com.musala.kb.provider.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.musala.kb.provider.service.ConsoleSpammer;

@Component (
    immediate = true
)
public class ConsoleSpammerImpl implements ConsoleSpammer {

	public static final String LOG_ID = "com.musala.kb.provider.ConsoleSpammerImpl";
	
	private Map<String, String> properties = new HashMap<String, String>();
	
//	lifecycle
	
	@Activate
	private void activate(ComponentContext context) {
		System.out.println(String.format("[%s] - Bundle has started!", LOG_ID));
	}
	
	@Deactivate
	private void deactivate(ComponentContext context) {
		System.out.println(String.format("[%s] - Bundle has stopped!", LOG_ID));
	}
		
//	Service methods implementation
	
	@Override
	public void setProperties(Map<String, String> properties) {
		System.out.println(String.format("[%s] - Setting properties !", LOG_ID));
		
		this.properties = properties;
		
		System.out.println(String.format("[%s] - Properties Setted !", LOG_ID));		
	}

	@Override
	public void startSpammingConsole() {
		System.out.println(String.format("[%s] - Spamming console Starting !", LOG_ID));
		
		properties.entrySet().stream().forEach(e -> System.out.println(String.format("%s: %s", e.getKey(), e.getValue())));
		
		System.out.println(String.format("[%s] - Spamming console Started !", LOG_ID));
	}

}
