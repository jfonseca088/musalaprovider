package com.musala.kb.provider.service;

import java.util.Map;

public interface ConsoleSpammer {
	
	void setProperties(Map<String, String> map);
	
	void startSpammingConsole();

}
